### kakoune-scribble

---

[Scribble](https://docs.racket-lang.org/scribble/index.html) support for the [Kakoune](https://kakoune.org/) editor.


![](/res/kakoune-scribble.png)


#### Install  

---


```
mv scribble.kak ~/.config/kak/autoload/scribble.kak
```

[plug.kak](https://github.com/andreyorst/plug.kak)  
```
plug "KJ_Duncan/kakoune-scribble.kak" domain "bitbucket.org"
```

Supported file extensions: `(scrbl)`


##### TODO  

- [x] patience problems are inevitable  


Modification of the original kakoune file: [scheme.kak](https://github.com/mawww/kakoune/blob/master/rc/filetype/scheme.kak).  

Parenthesis, brackets, and braces highlighting.  
Just change `0:{colour}` to suit your theme  
`add-highlighter shared/scribble/code/ regex [()\[\]{}] 0:yellow`  

Kakoune default colour [codes](https://github.com/mawww/kakoune/blob/master/colors/default.kak):  

* value: red
* type,operator: yellow
* variable,module,attribute: green
* function,string,comment: cyan
* keyword: blue
* meta: magenta
* builtin: default
 
---


##### Kakoune General Information #####


Work done? Have some fun. Share any improvements, ideas or thoughts with the community either directly on [discuss.kakoune](https://discuss.kakoune.com/) or vicariously through this repo/issue/pull/email and I will post it on your behalf as anonymous unless otherwise directed.  
  
Kakoune is an open source modal editor. The source code lives on github [mawww/kakoune](https://github.com/mawww/kakoune#-kakoune--).  
A discussion on the repository structure of _’community plugins’_ for Kakoune can be reviewed here: [Standardi\(s|z\)ation of plugin file-structure layout #2402](https://github.com/mawww/kakoune/issues/2402).  
Read the Kakoune wiki for information on install options via a [Plugin Manager](https://github.com/mawww/kakoune/wiki/Plugin-Managers).  
  
Thank you to the Kakoune community 180+ contributors for a great modal editor in the terminal. I use it daily for the keyboard shortcuts.  
  
  
---
  
That's it for the readme, anything else you may need to know just pick up a book and read it [Polar Bookshelf](https://github.com/burtonator/polar-bookshelf). Thanks all. Bye.
