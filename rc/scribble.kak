# https://docs.racket-lang.org/scribble/index.html
# ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾

# --------------------------------------------------------------------------------------------------- #
# kak colour codes; value:red, type,operator:yellow, variable,module,attribute:green,
#                   function,string,comment:cyan, keyword:blue, meta:magenta, builtin:default
# --------------------------------------------------------------------------------------------------- #

# Detection
# ‾‾‾‾‾‾‾‾‾

hook global BufCreate .*[.](scrbl) %{
  set-option buffer filetype scribble
}

# Initialization
# ‾‾‾‾‾‾‾‾‾‾‾‾‾‾

hook global WinSetOption filetype=scribble %{
  require-module scribble

  # set-option buffer extra_word_chars '_' '-' '!' '%' '?' '<' '>' '='

  set-option buffer comment_line '@;'
  set-option buffer comment_block_begin '@;{'
  set-option buffer comment_block_end '}'

  hook window ModeChange pop:insert:.* -group scribble-trim-indent lisp-trim-indent
  hook window InsertChar \n -group scribble-indent lisp-indent-on-new-line

  hook -once -always window WinSetOption filetype=.* %{ remove-hooks window scribble-.+ }
}

hook -group scribble-highlight global WinSetOption filetype=scribble %{
  add-highlighter window/scribble ref scribble
  hook -once -always window WinSetOption filetype=.* %{ remove-highlighter window/scribble }
}

# --------------------------------------------------------------------------------------------------- #
provide-module scribble %§

require-module lisp

add-highlighter shared/scribble regions
add-highlighter shared/scribble/code default-region group
add-highlighter shared/scribble/string region '"' (?<!\\)(\\\\)*" fill string
add-highlighter shared/scribble/comment region '@;' '$' fill comment

# --------------------------------------------------------------------------------------------------- #
# link to regular expression <https://regex101.com/r/uZafls/1> as at 24/03/2019
add-highlighter shared/scribble/code/ regex %{(?<!\w)(?<cmd>@[a-z-]+)\b} cmd:blue

# --------------------------------------------------------------------------------------------------- #
# link to regular expression <https://regex101.com/r/IPJYnf/1> as at 24/03/2019
add-highlighter shared/scribble/code/ regex %{(?<!\w)(?<ms>#:\w+[-\w]+)} ms:cyan

# --------------------------------------------------------------------------------------------------- #
# link to regular expression <https://regex101.com/r/8Dav5v/1> as at 24/03/2019
add-highlighter shared/scribble/code/ regex %{(?:'\w+[^'\v\]]+?')|(?<da>'\S+)\]|(?<ta>\B'[^'\s]\S+)} da:green ta:green

# --------------------------------------------------------------------------------------------------- #
# change 0:{colour} to suit your colour theme
add-highlighter shared/scribble/code/ regex [()\[\]{}|] 0:yellow
# --------------------------------------------------------------------------------------------------- #
§
